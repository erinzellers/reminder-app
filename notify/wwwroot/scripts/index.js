﻿var app = angular.module('notify', [
    'ui.router',
    'ngMaterial',
    'ngAnimate',
    "ngAria",
    "ngMessages",
    "mdPickers"
]).run([
    "$state",
    function ($state) {
        $state.go('login');
    }
]);

app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
        .state('login',
            {
                url: '/login',
                //templateUrl: 'templates/login.html',
                //controller: 'loginController as ctrl',
                views: {
                    'content@': {
                        templateUrl: 'templates/login.html',
                        controller: 'loginController as ctrl'
                    }		
                }
            }
        ).state('landingPage',
            {
                url: '/main',
                //templateUrl: 'templates/landing.html',
                //controller: 'indexController as ctrl',
                views: {
                    'nav@': {
                        templateUrl: 'templates/nav.html'
                    },
                    'content@': {
                        templateUrl: 'templates/inputs.html',
                        controller: 'indexController as ctrl'
                    }
                }
            }
        ).state('existing',
            {
                url: '/reminders',
                //templateUrl: 'templates/reminders.html',
                //controller: 'remindersController as ctrl'
                views: {
                    'nav@': {
                        templateUrl: 'templates/nav.html'
                    },
                    'content@': {
                        templateUrl: 'templates/reminders.html',
                        controller: 'remindersController as ctrl'
                    }
                }
            }
        ).state('createProfile',
            {
                url: '/profile',
                //templateUrl: 'templates/reminders.html',
                //controller: 'remindersController as ctrl'
                views: {
                    
                    'content@': {
                        templateUrl: 'templates/profileForm.html',
                        controller: 'profileController as ctrl'
                    }
                }
            }
        );
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    //$urlRouterProvider.otherwise('/');
});


