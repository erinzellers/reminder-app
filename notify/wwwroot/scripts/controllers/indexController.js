﻿app.controller('indexController',
    function ($scope, $state, $http, $mdToast, $mdpDatePicker, $mdpTimePicker, userFactory) {

        $scope.reminder = {};
        $scope.reminder.date = new Date();
        $scope.reminder.phone = userFactory.get().phone;

        this.showDatePicker = function (ev) {
            $mdpDatePicker($scope.reminder.date, {
                targetEvent: ev
            }).then(function (selectedDate) {
                $scope.reminder.date = selectedDate;
            });
        };
        

        this.showTimePicker = function (ev) {
            $mdpTimePicker($scope.reminder.date, {
                targetEvent: ev
            }).then(function (selectedDate) {
                $scope.reminder.date = selectedDate;
            });
        };

        $scope.clearReminder = function() {
            $scope.reminder = {};
        };

        $scope.showCustomToast = function() {
            $mdToast.show({
                hideDelay: 3000,
                position: 'top right',
                controller: 'toastController',
                templateUrl: '../../templates/toast.template.html'
            });
        };

        console.log(userFactory.get());

        $scope.createReminder = function () {            

            console.log($scope.reminder);

            var phoneValidation = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

            if ($scope.reminder.phone.match(phoneValidation)) {

                let utc = moment.utc($scope.reminder.date).toDate();

                0
                $http({
                    method: 'POST',
                    url: 'api/reminders',
                    data: {
                        reminderTime: moment(utc).local().format('YYYY-MM-DD HH:mm:ss'),
                        message: $scope.reminder.message,
                        phone: $scope.reminder.phone
                    }
                }).then(function successCallback(resp) {
                        $scope.reminder = {};
                        $scope.reminder.date = new Date();
                        //success toast
                        $scope.showCustomToast();
                        //redirect to entry page
                    },
                    function errorCallback(resp) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                        alert('error: ' + resp);
                    });
            }

        };

        

       


    });
