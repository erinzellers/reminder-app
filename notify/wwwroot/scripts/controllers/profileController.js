﻿app.controller('profileController',
    function ($scope, $state, $http, $mdToast, userFactory) {

        $scope.newUser = {};

        $scope.showToast = function (errorResp) {

            let errorMessage = '';

            if (angular.isObject(errorResp)) {
                errorMessage = 'the email and phone you entered already are in use';
            } else if (errorResp.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)  ) {
                errorMessage = 'the phone number you entered is already in use';
            } else {
                errorMessage = 'the email you entered is already in use';
            }

            $mdToast.show(
                $mdToast.simple()
                    .textContent(errorMessage)
                    .position('top')
                    .hideDelay(4000)
            );
        }

        $scope.createUserProfile = function () {
            //check if user e-mail / phone already exists 

            //if it does return error

            //else add user and redirect to create page
            $http({
                method: 'POST',
                url: 'api/users',
                data: {
                    name: $scope.newUser.name,
                    email: $scope.newUser.email,
                    phone: $scope.newUser.phone
                }
            }).then(function successCallback(resp) {

                    //add user to user service to then send to create page
                    userFactory.set(resp.data);
                    //redirect to create page
                    $state.go('landingPage');                    
                   
                    //success toast
                    //$scope.showCustomToast();                   
                    
                },
                function errorCallback(resp) {
                    
                    console.log(resp.data);
                    
                    $scope.showToast(resp.data);


                    

                });
        }

    });