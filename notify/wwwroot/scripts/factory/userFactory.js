﻿app.factory('userFactory',
    function() {

        var userData = {
            
        };

        return {
            get: function () {
                return userData;
            },
            set: function (user) {
                userData = user;
            }
        };

    });