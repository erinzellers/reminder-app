﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using notify.models;

namespace notify.Models
{
    public class notifyContext : DbContext
    {
        public notifyContext (DbContextOptions<notifyContext> options)
            : base(options)
        {
        }

        public DbSet<notify.models.reminder> reminder { get; set; }

        public DbSet<notify.models.user> user { get; set; }
    }
}
