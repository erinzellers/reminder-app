using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using notify.Models;
using notify.models;
using Newtonsoft.Json;

namespace notify.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    public class usersController : Controller
    {
        private readonly notifyContext _context;

        public usersController(notifyContext context)
        {
            _context = context;
        }

        // GET: api/users
        [HttpGet]
        public IEnumerable<user> Getuser()
        {
            return _context.user;
        }

        // GET: api/users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getuser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.user.SingleOrDefaultAsync(m => m.id == id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putuser([FromRoute] int id, [FromBody] user user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!userExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/users
        [HttpPost]
        public async Task<IActionResult> Postuser([FromBody] user user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userPhoneExists(user) && userEmailExists(user))
            {
                return BadRequest(JsonConvert.SerializeObject(user));
            }

            if ( userEmailExists(user))
            {
                return BadRequest(JsonConvert.SerializeObject(user.email));
            }

            if (userPhoneExists(user))
            {
                return BadRequest(JsonConvert.SerializeObject(user.phone));
            }            

            _context.user.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getuser", new { id = user.id }, user);
        }

        // DELETE: api/users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deleteuser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.user.SingleOrDefaultAsync(m => m.id == id);
            if (user == null)
            {
                return NotFound();
            }

            _context.user.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        private bool userExists(int id)
        {
            return _context.user.Any(e => e.id == id);
        }

        private bool userEmailExists(user user)
        {           
            return _context.user.Any(e => e.email == user.email);
        }

        private bool userPhoneExists(user user)
        {           
            return _context.user.Any(e => e.phone == user.phone);
        }
    }
}