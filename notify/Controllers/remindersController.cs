using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using notify.Models;
using notify.models;
using notify.service;
using Newtonsoft.Json.Linq;

namespace notify.Controllers
{
    [Produces("application/json")]
    [Route("api/reminders")]
    public class remindersController : Controller
    {
        private readonly notifyContext _context;

        public remindersController(notifyContext context)
        {
            _context = context;
        }

        // GET: api/reminders
        [HttpGet]
        public IEnumerable<reminder> Getreminder()
        {
            return _context.reminder;
        }

        // GET: api/reminders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getreminder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reminder = await _context.reminder.SingleOrDefaultAsync(m => m.id == id);

            if (reminder == null)
            {
                return NotFound();
            }

            return Ok(reminder);
        }

        // PUT: api/reminders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putreminder([FromRoute] int id, [FromBody] reminder reminder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reminder.id)
            {
                return BadRequest();
            }

            _context.Entry(reminder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!reminderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/reminders
        [HttpPost]
        public async Task<IActionResult> Postreminder([FromBody] reminder r)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }                

                var reminder = new reminder
                {
                    message = r.message,
                    phone = r.phone,
                    createdAt = DateTime.Now,
                    reminderTime = r.reminderTime
                };


                var schedule = new service.cronService();

                schedule.hangFire(reminder, reminder.reminderTime, reminder.createdAt);

                _context.reminder.Add(reminder);
                await _context.SaveChangesAsync();

                return CreatedAtAction("Getreminder", new { id = reminder.id }, reminder);
            }
            catch (Exception e)
            {
                return BadRequest(e);
                throw;
            }
        }

        // DELETE: api/reminders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletereminder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reminder = await _context.reminder.SingleOrDefaultAsync(m => m.id == id);
            if (reminder == null)
            {
                return NotFound();
            }

            _context.reminder.Remove(reminder);
            await _context.SaveChangesAsync();

            return Ok(reminder);
        }

        private bool reminderExists(int id)
        {
            return _context.reminder.Any(e => e.id == id);
        }
    }
}