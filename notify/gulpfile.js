﻿/// <binding BeforeBuild='copy-assets' />
"use strict";

var _ = require('lodash'),
    gulp = require('gulp');

gulp.task('copy-assets', function () {
    var assets = {
        js: [            
            './node_modules/angular/angular.js',
            './node_modules/jquery/dist/jquery.js',
            './node_modules/angular-ui-router/release/angular-ui-router.js',
            './node_modules/angular-aria/angular-aria.js',
            './node_modules/angular-animate/angular-animate.js',
            './node_modules/angular-material/angular-material.js',
            './node_modules/moment/moment.js',
            './node_modules/mdPickers/dist/mdPickers.js',
            './node_modules/angular-messages/angular-messages.js'
        ],
        css: [
            './node_modules/angular-material/angular-material.css',
            './node_modules/mdPickers/dist/mdPickers.css'
        ]
    };
    _(assets).forEach(function (assets, type) {
        gulp.src(assets).pipe(gulp.dest('./wwwroot/lib/' + type));
    });
});