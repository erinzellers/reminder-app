﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notify.models
{
    public class reminder
    {
        public int id { get; set; }
        public string phone { get; set; }
        public DateTime reminderTime { get; set; }
        public string timeZone { get; set; }
        public DateTime createdAt { get; set; }
        public string message { get; set; }
        public int userId { get; set; }
        public virtual user user { get; set; }
    }
}
