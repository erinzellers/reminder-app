﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using notify.Models;

namespace notify.Migrations
{
    [DbContext(typeof(notifyContext))]
    [Migration("20170817202826_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("notify.models.reminder", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("createdAt");

                    b.Property<string>("message");

                    b.Property<string>("phone");

                    b.Property<DateTime>("reminderTime");

                    b.Property<string>("timeZone");

                    b.HasKey("id");

                    b.ToTable("reminder");
                });
        }
    }
}
