﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using notify.models;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;


namespace notify.service
{
    public class ApiService
    {
        private const string twilioPhoneNumber = "+15024953418";
        private const string sid = "AC20c0ce9ab24665b2a0dc8a7b28d4ce88";
        private const string authToken = "06a177da97c8d78964604463bd1afd07";

        public async Task<string> GetExternalResponse(reminder reminder)
        {            
            TwilioClient.Init(sid, authToken);

            var message = await MessageResource.CreateAsync(
                to: new PhoneNumber(reminder.phone),
                from: new PhoneNumber(twilioPhoneNumber),
                body: reminder.message);


            return message.Body;
            //Console.WriteLine(message.Sid);
        }
    }
}
