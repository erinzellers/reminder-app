﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using notify.Models;
using notify.models;

namespace notify.service
{
    public class cronService
    {
        public void hangFire(reminder reminder, DateTime reminderTime, DateTime reminderCreatedTime)
        {
            try
            {
                var delay = reminderTime.Subtract(reminderCreatedTime).TotalMinutes;

                var twilio = new ApiService();
                BackgroundJob.Schedule(() => twilio.GetExternalResponse(reminder), TimeSpan.FromMinutes(delay));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
